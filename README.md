
<!-- README.md is generated from README.Rmd. Please edit that file -->

# ROAbots

<!-- badges: start -->

<!-- badges: end -->

The goal of ROAbots is to analyze the `robots.txt` files of Open Access
journals.

## Installation

You can install the released version of ROAbots with:

``` r
devtools::install_gitlab("gittaca/ROAbots")
```

## Data gathering

First, we load the previously cached, already `checked` robots.txt
permissions. We want to refresh this cache partially, so we extract and
remove `old` data from it. Then, we obtain the Directory of Open Access
Journals data we are interested in. Lastly, we merge old and new data
into the a list of journals from which we sample a fraction of
`robots.txt` files to download.

``` r
checked <- readr::read_rds(CACHE)
old <- checked %>%
  dplyr::filter(is.na(Date) | Date < Sys.Date() - OLD) %>%
  dplyr::slice_tail(order_by = Date, prop = SAMPLE)
checked <- dplyr::anti_join(checked, old, by = "Journal URL")

unchecked <- readr::read_csv(DATA) %>%
  dplyr::select(`Journal URL`) %>%
  dplyr::left_join(checked, by = "Journal URL") %>%
  dplyr::filter(is.na(Domain)) %>%
  dplyr::slice_sample(prop = SAMPLE)

to_check <- dplyr::add_row(unchecked, old)
```

From the `unchecked` and `old` data, we check 1% of URLs each.

``` r
robots.txt <- to_check %>%
  dplyr::select(`Journal URL`) %>%
  dplyr::mutate(Domain = purrr::map_chr(`Journal URL`, ~ httr::parse_url(.x)$hostname)) %>%
  
  # Avoid robotstxt parsing errors, see https://github.com/ropensci/robotstxt/pull/59
  dplyr::anti_join(UNPARSABLE, by = "Domain") %>%
  
  dplyr::mutate(
    Content = purrr::map(Domain, ~ download_robots_txt(.x)),
    robots.txt = purrr::map(Content, ~ robotstxt::parse_robotstxt(.x)),
    Valid = purrr::map_lgl(Content, ~ check_validity(.x)),
    
    # Enable refreshing of already checked but old data
    Date = Sys.Date()
  )
#> Error in request_handler_handler(request = request, handler = on_server_error,  : 
#>   Event: on_server_error
#> Error in curl::curl_fetch_memory(url, handle = handle) : 
#>   Could not resolve host: jurnal.stainponorogo.ac.id
#> Error in curl::curl_fetch_memory(url, handle = handle) : 
#>   SSL: no alternative certificate subject name matches target host name '194.27.41.48'
```

From all valid rulesets, we extract the disallow rules.

``` r
permissions <- robots.txt %>%
  dplyr::filter(Valid) %>%
  dplyr::mutate(
    Permissions = purrr::map(robots.txt, ~ .x$permissions),
    Disallow_rules = purrr::map(Permissions, ~ dplyr::filter(.x, field == "Disallow")),
    Disallows_any = purrr::map_lgl(Disallow_rules, ~ any(!is.na(.x))),
    Disallows_all = purrr::map_lgl(Disallow_rules, ~ .x$useragent == "*" && .x$value == "/")
  ) %>%
  dplyr::add_row(checked)

permissions %>%
  dplyr::select(-Content, -robots.txt) %>%
  readr::write_rds(CACHE, compress = "xz")
```

## Bots with the most disallow rules (top 100)

We visualise some aspects of the disallow rules as
[wordclouds](https://www.littlemissdata.com/blog/wordclouds).

``` r
rules <- permissions %>%
  dplyr::filter(Disallows_any == TRUE) %>%
  tidyr::unnest(Disallow_rules) %>%
  dplyr::filter(value != "")
  # Remove empty rules, as those are separator lines in the robots.txt file

rules %>%
  
  dplyr::filter(useragent != "*") %>%
  # As this will be the most popular disallow rule for non-Content URL paths,
  # it carries no information for investigating the specifically blocked bots.
  
  dplyr::select(useragent) %>%
  dplyr::mutate(useragent = stringr::str_to_lower(useragent)) %>% 
  dplyr::count(useragent, sort = TRUE) %>%
  dplyr::slice_max(n, n = 100) %>%
  wordcloud2::wordcloud2() %>% 
  htmlwidgets::saveWidget("wc_robots.html", selfcontained = FALSE)

webshot::webshot("wc_robots.html", "wc_robots.png", vwidth = 800, vheight = 600, delay = 2)
```

<img src="man/figures/README-wordcloud-disallowed-bots-1.png" width="100%" />

## Top 100 most often disallowed paths

``` r
rules %>%
  dplyr::select(value) %>%
  dplyr::count(value, sort = TRUE) %>%
  dplyr::slice_max(n, n = 100) %>%
  wordcloud2::wordcloud2() %>% 
  htmlwidgets::saveWidget("wc_paths.html", selfcontained = FALSE)

webshot::webshot("wc_paths.html", "wc_paths.png", vwidth = 600, vheight = 400, delay = 2)
```

<img src="man/figures/README-wordcloud-disallowed-paths-1.png" width="100%" />

## Journal URLs that have no valid `robots.txt`

``` r
robots.txt %>%
  dplyr::filter(!Valid) %>%
  dplyr::select(Domain, Content) %>%
  dplyr::distinct(Domain) %>%
  dplyr::arrange(Domain) %>%
  knitr::kable()
```

| Domain                         |
| :----------------------------- |
| 194.27.41.48                   |
| cjs.mubabol.ac.ir              |
| hehp.modares.ac.ir             |
| jurnal.stainponorogo.ac.id     |
| www.curriculosemfronteiras.org |
| www.irjo.org                   |

## Journals that indiscriminately `Disallow: /` for `User-agent: *`

Although these journals may have specific `Allow` rules for known
bots/spiders, they necessitate new search engines’ operators to actively
reach out to be added. That IMHO risks inhibiting innovation, and runs
counter to the first [“Ethical Site Owner”
rule](https://towardsdatascience.com/ethics-in-web-scraping-b96b18136f01)
as suggested by James Densmore.

To account for webmaster `Considerateness`, we introduce such a score
(higher is better) by counting the number of bots/spiders that are
explicitly provided with rules.

``` r
permissions %>%
  dplyr::filter(Disallows_all) %>%
  tidyr::unnest(Disallow_rules) %>%
  dplyr::filter(useragent != "*" & value != "/") %>%
  dplyr::add_count(Domain, useragent, name = "Considerateness") %>%
  dplyr::select(Domain, Considerateness) %>%
  dplyr::arrange(Considerateness) %>%
  dplyr::distinct(Domain, .keep_all = TRUE) %>%
  knitr::kable()
```

| Domain                    | Considerateness |
| :------------------------ | --------------: |
| presidencia.gencat.cat    |               1 |
| www.ria.nomos.de          |               1 |
| jbcs.sbq.org.br           |               1 |
| www.medicalexpress.net.br |               1 |
| www.reme.org.br           |               1 |
| sleepscience.org.br       |               1 |
| ibvs.konkoly.hu           |               5 |
| www.biotaxa.org           |               6 |
| www.sgp.geodezja.org.pl   |              15 |
| www.sciencedirect.com     |             232 |
